#include <Servo.h>

enum  PumpDirection{
  Backwards, Forward
};

int tempPin = A1;
int servoPin = 9;
int servoPos = 90;
int incomingByte = 0;
int charLeft = 97; // A
int charRight = 100; // D
int charStop = 115; // S
int charFanStart = 102;  //F
int charFanStop = 114; // R
int charPeltierStart = 103; // G
int charPeltierStop = 116; // T
int charLogTemp = 108; // L
int charLogTempStop = 111; // O
int charManageTemp = 109; // M
int charManageTempOff = 106; // J
int charPumpOn = 59; // ;
int charPumpOff = 112; // P
int pumpCounter = 0;
int pumpDirection = Forward;

int8_t servoDetached = 1;
int8_t logTemperature = 0;
int8_t manageTemp = 0;
int8_t pumpOn = 0;
int peltierPin = 4;
int fanPin = 3;
float tempCeiling = 38.0;
float tempFloor = 36.0;

int amplitude = 60;
int motorDegreesPerCallback = 10;
int intervalBetweenCallbacks = 1000;

Servo servo;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(19200);
  servo.attach(servoPin);
  servoDetached = 0;
}

float readTemperature() {
 int reading = analogRead(tempPin);
 
 float voltage = reading * 5.0;
 voltage /= 1024.0; 
 float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
                                               //to degrees ((voltage - 500mV) times 100)
 
 return temperatureC;
}

void manageBottleClimate() {
  float currentTemperature = readTemperature();
  if ( currentTemperature > tempCeiling ) {
    digitalWrite( fanPin, HIGH );
    digitalWrite( peltierPin, LOW );
  }
  if ( currentTemperature < tempFloor ) {
    digitalWrite( fanPin, LOW );
    digitalWrite( peltierPin, HIGH );
  }
}

void pump( ) {
  int callbacksPerAmplitute = amplitude / motorDegreesPerCallback;
  if ( pumpCounter == callbacksPerAmplitute ) {
    pumpDirection = Backwards;
  }

  if ( pumpCounter == 0 ) {
    pumpDirection = Forward;
  }  

  if ( pumpDirection == Backwards ) {
    servoPos-=motorDegreesPerCallback;
    pumpCounter--;
  } else {
    servoPos+=motorDegreesPerCallback;
    pumpCounter++;
  }
  servo.write ( servoPos );
}

void loop() {
  // put your main code here, to run repeatedly:

  if ( Serial.available() > 0 ) {
    incomingByte = Serial.read();
    //Serial.println(incomingByte, DEC);
    if ( incomingByte == charLeft || incomingByte == charRight ) {
      if ( servoDetached == 1 ) {
        servoDetached = 0;
        servo.attach(servoPin);
        Serial.println( "servo started" );
      }
    }
    if ( incomingByte == charLeft ) {
      if ( servoPos > 0 ) {
        servoPos = servoPos -motorDegreesPerCallback;
        Serial.println("servo speed");Serial.println( servoPos );
        servo.write(servoPos);
      }
    }
    if ( incomingByte == charRight ) {
      if ( servoPos < 180 ) {
        servoPos = servoPos + motorDegreesPerCallback;
        Serial.println("servo speed");Serial.println( servoPos );       
        servo.write(servoPos);
      }
    }
    if ( incomingByte == charStop ) {
      servo.detach();
      servoDetached = 1;
      Serial.println("servo stopped");
    }
    if ( incomingByte == charFanStart ) {
      Serial.println("Starting fan");
      digitalWrite( fanPin, HIGH );
    }
    if ( incomingByte == charFanStop ) {
      Serial.println("Stopping fan");
      digitalWrite( fanPin, LOW );
    }
    if ( incomingByte == charPeltierStart ) {
      Serial.println("Starting peltier element");
      digitalWrite( peltierPin, HIGH );
    }
    if ( incomingByte == charPeltierStop ) {
      Serial.println("Stopping peltier element");
      digitalWrite( peltierPin, LOW );
    }
    if ( incomingByte == charLogTemp ) {
      Serial.println("Starting to log temperature");
      logTemperature =  1;
    }
    if ( incomingByte == charLogTempStop ) {
      Serial.println("Stop to log temperature");
      logTemperature = 0;
    }
    if ( incomingByte == charManageTemp ) {
      Serial.println("Start to manage temperature");
      manageTemp = 1;
    }
    if ( incomingByte == charManageTempOff ) {
      Serial.println("Stop to manage temperature");
      manageTemp = 0;
    }
    if ( incomingByte == charPumpOn ) {
      Serial.println ( "Switching pump on" );
      pumpOn = 1;
    }
    if ( incomingByte == charPumpOff ) {
      Serial.println(  "Swithcing pump off" );
      pumpOn = 0;
    }
  }
  //servo.write(servoPos);
  delay(intervalBetweenCallbacks);
  if ( logTemperature == 1 ) {
    Serial.print(readTemperature()); Serial.println(" degrees C");    
  }
  if ( manageTemp == 1 ) {
    manageBottleClimate();    
  }
  if ( pumpOn == 1 ) {
    pump();
  }
}
